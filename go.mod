module gitlab.com/ilyasiv2003/mailjet-svc

go 1.15

require (
	github.com/asaskevich/govalidator v0.0.0-20210307081110-f21760c49a8d // indirect
	github.com/assembla/cony v0.3.2
	github.com/cockroachdb/errors v1.8.2
	github.com/go-ozzo/ozzo-validation v3.6.0+incompatible
	github.com/mailjet/mailjet-apiv3-go v0.0.0-20201009050126-c24bc15a9394
	github.com/sirupsen/logrus v1.8.0
	github.com/spf13/viper v1.3.2
	github.com/streadway/amqp v1.0.0
)
