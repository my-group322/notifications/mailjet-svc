package services

import (
	"github.com/mailjet/mailjet-apiv3-go"
	"github.com/streadway/amqp"
)

func (s *service) processDelivery(*amqp.Delivery) {
	payload := []mailjet.InfoMessagesV31{{
		From: &mailjet.RecipientV31{
			Email: "salexnikolaevich@gmail.com",
			Name:  "Ilya",
		},
		To: &mailjet.RecipientsV31{
			mailjet.RecipientV31{
				Email: "salexnikolaevich@gmail.com",
				Name:  "Ilya",
			},
		},
		Subject:  "Greetings from Mailjet.",
		TextPart: "My first Mailjet email",
		HTMLPart: "<h3>Dear passenger 1, welcome to <a href='https://www.mailjet.com/'>Mailjet</a>!</h3><br />May the delivery force be with you!",
		CustomID: "AppGettingStartedTest"},
	}

	mail := mailjet.MessagesV31{Info: payload}
	res, err := s.mailjetCli.SendMailV31(&mail)
	if err != nil {
		s.log.WithError(err).Error("failed to sent message")
	}

	s.log.Info(res)
}
