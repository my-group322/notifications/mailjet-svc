package services

import (
	"github.com/mailjet/mailjet-apiv3-go"
	"github.com/sirupsen/logrus"
	"gitlab.com/ilyasiv2003/mailjet-svc/internal/config"
	"gitlab.com/ilyasiv2003/mailjet-svc/internal/rabbitmq"
)

type service struct {
	config      config.Config
	rabbitmqCli rabbitmq.Consumer
	log         *logrus.Logger
	mailjetCli  *mailjet.Client
}

func newService(cfg config.Config, cli rabbitmq.Consumer, logger *logrus.Logger, mail *mailjet.Client) *service {
	return &service{
		config:      cfg,
		rabbitmqCli: cli,
		log:         logger,
		mailjetCli:  mail,
	}
}

func Run(cfg config.Config, cli rabbitmq.Consumer, logger *logrus.Logger, mailCli *mailjet.Client) {
	newService(cfg, cli, logger, mailCli).run()
}

func (s *service) run() {
	for s.rabbitmqCli.Loop() {
		select {
		case msg := <-s.rabbitmqCli.Deliveries():
			go s.processDelivery(&msg)
		case err := <-s.rabbitmqCli.Consumer.Errors():
			s.log.WithError(err).Error("consumer error")
		case err := <-s.rabbitmqCli.Client.Errors():
			s.log.WithError(err).Error("client error")
		}
	}
}
