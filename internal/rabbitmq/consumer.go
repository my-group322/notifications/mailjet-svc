package rabbitmq

import (
	"github.com/assembla/cony"
	"gitlab.com/ilyasiv2003/mailjet-svc/internal/config"
)

type Consumer struct {
	*cony.Client
	*cony.Consumer
}

func NewConsumer(cfg config.Config) Consumer {
	cli := cony.NewClient(
		cony.URL(cfg.RabbitMQUrl),
		cony.Backoff(cony.DefaultBackoff),
	)

	que := &cony.Queue{
		AutoDelete: true,
		Name:       "mailQueue",
	}
	exc := cony.Exchange{
		Name:       "notifications",
		Kind:       "fanout",
		AutoDelete: true,
	}
	bnd := cony.Binding{
		Queue:    que,
		Exchange: exc,
		Key:      "",
	}
	cli.Declare([]cony.Declaration{
		cony.DeclareQueue(que),
		cony.DeclareExchange(exc),
		cony.DeclareBinding(bnd),
	})

	consumer := cony.NewConsumer(
		que,
		cony.AutoAck(),
	)
	cli.Consume(consumer)

	return Consumer{
		Client: cli,
		Consumer: consumer,
	}
}
