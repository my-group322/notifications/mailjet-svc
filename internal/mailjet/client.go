package mailjet

import (
	"github.com/mailjet/mailjet-apiv3-go"
	"gitlab.com/ilyasiv2003/mailjet-svc/internal/config"
)

func NewClient(cfg config.Config) *mailjet.Client {
	return mailjet.NewMailjetClient(cfg.MailjetApiKey, cfg.MailjetSecretKey)
}
