package cli

import (
	"github.com/cockroachdb/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/ilyasiv2003/mailjet-svc/internal/config"
	"gitlab.com/ilyasiv2003/mailjet-svc/internal/mailjet"
	"gitlab.com/ilyasiv2003/mailjet-svc/internal/rabbitmq"
	"gitlab.com/ilyasiv2003/mailjet-svc/services"
)

func Run() {
	log := logrus.New()

	cfg, err := config.NewConfig()
	if err != nil {
		panic(errors.Wrap(err, "failed to setup config"))
	}

	rabbitmqCli := rabbitmq.NewConsumer(*cfg)
	mailjetCli := mailjet.NewClient(*cfg)

	services.Run(*cfg, rabbitmqCli, log, mailjetCli)
}
