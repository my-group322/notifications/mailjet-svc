package config

import (
	"github.com/cockroachdb/errors"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/spf13/viper"
	"os"
)

const envViperConfigFile = "VIPER_FILE"

type Config struct {
	RabbitMQUrl string `mapstructure:"rabbitmq_url"`

	MailjetApiKey    string `mapstructure:"mailjet_api_key"`
	MailjetSecretKey string `mapstructure:"mailjet_secret_key"`
}

func NewConfig() (*Config, error) {
	var config Config

	viperFilePath := os.Getenv(envViperConfigFile)
	if viperFilePath == "" {
		return nil, errors.New("failed to figure out viper file path")
	}

	viper.SetConfigFile(viperFilePath)

	if err := viper.ReadInConfig(); err != nil {
		return nil, errors.Wrap(err, "failed to read config")
	}

	if err := viper.Unmarshal(&config); err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal config")
	}

	return &config, config.validate()
}

func (c Config) validate() error {
	return validation.ValidateStruct(&c,
		validation.Field(&c.RabbitMQUrl, validation.Required),
		validation.Field(&c.MailjetApiKey, validation.Required),
		validation.Field(&c.MailjetSecretKey, validation.Required),
	)
}
